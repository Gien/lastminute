package com.lastminute.api;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

import java.util.Date;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.junit.Test;

public class ApplicationIntegrationTest {
	private static final String NO_FLIGHTS_AVAILABLE = "No flights available";
	
	@Test
	public void App_result() throws Exception{
		String[] args = {"CPH", "FRA", "3", "31/12/2017"};
		assertNotEquals(Application.execution(args), NO_FLIGHTS_AVAILABLE);
	}
	
	@Test
	public void App_resultNo() throws Exception{
		String[] args = {"***", "***", "3", "31/12/2017"};
		assertEquals(Application.execution(args), NO_FLIGHTS_AVAILABLE);
	}
	
	@Test
	public void App_ExceptionNumberParameters() throws Exception{
		String[] args = {"***", "***", "31/12/2017"};
		try{
			Application.execution(args);
		}catch(Exception e){
			assertThat(e).isNotNull();
		}
	}
	
	@Test
	public void App_ExceptionNumber() throws Exception{
		String[] args = {"***", "***", "***", "31/12/2017"};
		try{
			Application.execution(args);
			fail();
		}catch(Exception e){
			assertThat(e).isNotNull();
		}
	}
	
	@Test
	public void App_noFlightsAvailable() throws Exception{
		String[] args = {"CDG", "FRA", "3", "31/12/2017"};
		assertEquals(Application.execution(args), NO_FLIGHTS_AVAILABLE);
	}
	
	@Test
	public void App_Example1() throws Exception{
		
		DateTime date = new DateTime();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
		String[] args = {"AMS", "FRA", "1", formatter.print(date.plusDays(50))};
		String result = Application.execution(args);
		System.out.println("*** Excample 1 (1 passenger, 31 days to the departure date, flying AMS -> FRA):" + result);
		
		if(!(result.contains("TK2372") && result.contains("priceTotal: 157,60"))){
			fail();
		}
		
		if(!(result.contains("TK2659") && result.contains("priceTotal: 198,40"))){
			fail();
		}
		
		if(!(result.contains("LH5909") && result.contains("priceTotal: 90,40"))){
			fail();
		}

		assertNotEquals(result, NO_FLIGHTS_AVAILABLE);
	}
	
	@Test
	public void App_Example2() throws Exception{
		
		DateTime date = new DateTime();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
		String[] args = {"LHR", "IST", "3", formatter.print(date.plusDays(15))};
		String result = Application.execution(args);
		System.out.println("*** Excample 2 (3 passengers, 15 days to the departure date, flying LHR -> IST):" + result);
		
		if(!(result.contains("TK8891") && result.contains("priceTotal: 900,00"))){
			fail();
		}
		
		if(!(result.contains("LH1085") && result.contains("priceTotal: 532,80"))){
			fail();
		}

		assertNotEquals(result, NO_FLIGHTS_AVAILABLE);
	}
	
	@Test
	public void App_Example3() throws Exception{
		
		DateTime date = new DateTime();
		DateTimeFormatter formatter = DateTimeFormat.forPattern("dd/MM/yyyy");
		String[] args = {"BCN", "MAD", "2", formatter.print(date.plusDays(2))};
		String result = Application.execution(args);
		System.out.println("*** Excample 3 (2 passengers, 2 days to the departure date, flying BCN -> MAD):" + result);
		
		if(!(result.contains("IB2171") && result.contains("priceTotal: 777,00"))){
			fail();
		}
		
		if(!(result.contains("LH5496") && result.contains("priceTotal: 879,00"))){
			fail();
		}

		assertNotEquals(result, NO_FLIGHTS_AVAILABLE);
	}
	
}
