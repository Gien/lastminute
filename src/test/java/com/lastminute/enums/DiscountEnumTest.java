package com.lastminute.enums;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.lastminute.core.enums.DiscountEnum;

public class DiscountEnumTest {

	int CORRECT_VALUE = 16;
	int WRONG_VALUE = 69;
	double CORRECT_DISCOUNT = 1.2;
	double WRONG_DISCOUNT = 69;
	DiscountEnum correcEnum = DiscountEnum.DISCOUNT_16;
	DiscountEnum wrongEnum = DiscountEnum.DISCOUNT_31;

	@Test
	public void getValue_WithCorrectValue() {
		assertTrue(correcEnum.getValue()==CORRECT_VALUE);
	}
	
	
	@Test
	public void getValue_WrongValue() {
		assertTrue(wrongEnum.getValue()!=WRONG_VALUE);
	};
	
	
	@Test
	public void getDiscountByValue_WithCorrectValue(){
		assertTrue(DiscountEnum.getDiscountByValue(8)==correcEnum);	
	}
	
	
	@Test
	public void getDiscountByValue_WrongValue(){
		assertTrue(DiscountEnum.getDiscountByValue(60)==wrongEnum);
	}

	@Test
	public void getDiscount_WithCorrectValue() {
		assertTrue(correcEnum.getDiscount()==CORRECT_DISCOUNT);
	}
	
	
	@Test
	public void getDiscount_WrongValue() {
		assertTrue(wrongEnum.getDiscount()!=WRONG_DISCOUNT);
	};

}
