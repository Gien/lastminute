package com.lastminute.service.searchflight.impl;

import java.util.ArrayList;
import java.util.List;

import com.lastminute.core.dto.FlightDTO;
import com.lastminute.core.dto.FlightPriceDTO;
import com.lastminute.core.dto.request.SearchFlightDTO;
import com.lastminute.core.dto.response.FlightResultDTO;
import com.lastminute.core.enums.DiscountEnum;
import com.lastminute.core.util.DateUtils;
import com.lastminute.persistence.CsvFiles;
import com.lastminute.service.searchflight.ISearchFlightService;

/***
 * 
 * @author Gonzalo Nava
 *
 */
public class SearchFlightService implements ISearchFlightService {
	
	/*
	 * Mantenemos en memoria los precios 
	 */
	private List<FlightPriceDTO> allFligthPrices= new ArrayList<>();
	
	/***
	 * Return the result of find flights
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	@Override
	public List<FlightResultDTO> findFlights(SearchFlightDTO params) throws Exception{
		
		checkParams(params);
		
		List<FlightDTO> allFligths = this.toFlightsDTO(CsvFiles.readAllRecords(fullPathTo("flight-routes.csv")));
		allFligthPrices = this.toPricesDTO(CsvFiles.readAllRecords(fullPathTo("flight-prices.csv")));
		
		return getFlightByRoute(params, allFligths);
	}
	
	/***
	 * Get the path resource
	 * @param fileName
	 * @return
	 */
	private String fullPathTo(String fileName){
		String sPath = getClass().getClassLoader().getResource(fileName).getPath();
		if (sPath.substring(0, 1).equals("/")){
			return sPath.substring(1);
		}
	    return sPath;
	}
	
	/**
	 * Check the parameters
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	private void checkParams(SearchFlightDTO params) throws Exception{
		if (params == null) { setError("The params is not coming"); }
		
		if (params.getDateDeparture() == null) { setError("The departure date is not idicate"); }
		if (params.getDestiny() == null) { setError("The destiny is not idicate"); }
		if (params.getOrigin() == null) { setError("The origin date is not idicate"); }
		if (params.getPassengers() == 0) { setError("The passengers is not idicate"); }
	}

	/***
	 * Get all flights by origin and destination
	 * @param params
	 * @param allFligths
	 * @return
	 */
	private List<FlightResultDTO> getFlightByRoute(SearchFlightDTO params, List<FlightDTO> allFligths) {
		List<FlightResultDTO> resultsFlights = new ArrayList<>();
		for (FlightDTO flight: allFligths){
			if (flight.getOriginFlight().equals(params.getOrigin()) && 
					flight.getDestinyFlight().equals(params.getDestiny())){
				resultsFlights.add(new FlightResultDTO(flight, this.getPrice(flight.getCodeFlight()), params.getPassengers(), getDiscount(params.getDateDeparture())));
			}
		}
		return resultsFlights;
	}
	
	/***
	 * Get a price by code flight
	 * @param codeFlight
	 * @return
	 */
	private int getPrice(String codeFlight){
		if (!allFligthPrices.isEmpty()){
			for (FlightPriceDTO flightprice : allFligthPrices){
				if (flightprice.getCodeFlight().equals(codeFlight)){
					return Integer.parseInt(flightprice.getPriceFlight());
				}
			}
		}
		return 0;
	}
	
	/***
	 * Convert the obtained flight data to DTO
	 * @param flightsRoutes
	 * @return
	 */
	private List<FlightDTO> toFlightsDTO(List<List<String>> flightsRoutes){
		List<FlightDTO> result = new ArrayList<>();
		for (List<String> flightRoute: flightsRoutes){
			result.add(new FlightDTO(flightRoute.get(0),flightRoute.get(1),flightRoute.get(2)));
		}
		
		return result;
	}
	
	/***
	 * Convert the obtained price data to DTO
	 * @param flightsPrices
	 * @return
	 */
	private List<FlightPriceDTO> toPricesDTO(List<List<String>> flightsPrices){
		List<FlightPriceDTO> result = new ArrayList<>();
		for (List<String> flightPrice: flightsPrices){
			result.add(new FlightPriceDTO(flightPrice.get(0), flightPrice.get(1)));
		}
		return result;
	}
	
	/***
	 * Get discount by date
	 * @param departureDate
	 * @return
	 */
	private double getDiscount(String departureDate){
		int diffDays = Math.toIntExact(DateUtils.getDays(DateUtils.formatDate(departureDate)));
		
		return DiscountEnum.getDiscountByValue(diffDays).getDiscount();
	}

	/**
	 * Exceptions by logs
	 * @param message
	 * @throws Exception
	 */
	private void setError(String message) throws Exception {
		throw new Exception(message);
	}

}
