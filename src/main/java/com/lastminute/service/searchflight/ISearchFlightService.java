package com.lastminute.service.searchflight;

import java.util.List;

import com.lastminute.core.dto.request.SearchFlightDTO;
import com.lastminute.core.dto.response.FlightResultDTO;

/***
 * Interface Search Flights
 * @author Gonzalo Nava Mu�oz
 *
 */
public interface ISearchFlightService {

	/***
	 * Return the result of find flights
	 * @param params
	 * @return
	 * @throws Exception 
	 */
	List<FlightResultDTO> findFlights(SearchFlightDTO params) throws Exception;

}
