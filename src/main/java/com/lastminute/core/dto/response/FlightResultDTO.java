package com.lastminute.core.dto.response;

import java.text.DecimalFormat;

import com.lastminute.core.dto.FlightDTO;

public class FlightResultDTO {
	
	private FlightDTO fligth;
	private int pricePassenger;
	private double priceTotal;
	
	/***
	 * Constructor by flight
	 * @param fligth
	 * @param pricePassenger
	 * @param passengers
	 * @param discount
	 */
	public FlightResultDTO(FlightDTO fligth, int pricePassenger, int passengers, double discount){
		this.fligth = fligth;
		this.pricePassenger = pricePassenger;
		this.priceTotal = ((pricePassenger * discount) * passengers);
	}
	
	public FlightDTO getFligth() {
		return fligth;
	}
	public void setFligth(FlightDTO fligth) {
		this.fligth = fligth;
	}
	public int getPricePassenger() {
		return pricePassenger;
	}
	public void setPricePassenger(int pricePassenger) {
		this.pricePassenger = pricePassenger;
	}
	public double getPriceTotal() {
		return priceTotal;
	}
	public void setPriceTotal(double priceTotal) {
		this.priceTotal = priceTotal;
	}
	
	@Override
	public String toString(){
		DecimalFormat decimalformat = new DecimalFormat("#.00"); 
		return "{ fligth : "+ this.fligth.toString()+ ", " +
				"pricePassenger: " + decimalformat.format(pricePassenger) + " � , " +
				"priceTotal: " + decimalformat.format(priceTotal) + " � " +
				"}";
	}
}
