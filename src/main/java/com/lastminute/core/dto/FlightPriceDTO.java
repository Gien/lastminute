package com.lastminute.core.dto;

public class FlightPriceDTO {
	private String codeFlight;
	private String priceFlight;
	
	/***
	 * Constructor Flight Price
	 * @param codeFlight
	 * @param priceFlight
	 */
	public FlightPriceDTO (String codeFlight, String priceFlight){
		this.codeFlight = codeFlight;
		this.priceFlight = priceFlight;
	}
	
	public String getCodeFlight() {
		return codeFlight;
	}
	public void setCodeFlight(String codeFlight) {
		this.codeFlight = codeFlight;
	}
	public String getPriceFlight() {
		return priceFlight;
	}
	public void setPriceFlight(String priceFlight) {
		this.priceFlight = priceFlight;
	}
	
	@Override
	public String toString(){
		return "{ codeFlight : " + this.codeFlight + ", priceFlight : " + this.priceFlight + "}";
	}
}
