package com.lastminute.core.dto;

public class FlightDTO {
	private String originFlight;
	private String destinyFlight;
	private String codeFlight;
	
	/***
	 * Constructor of Flight
	 * @param originFlight
	 * @param destinyFlight
	 * @param codeFlight
	 */
	public FlightDTO(String originFlight, String destinyFlight, String codeFlight){
		this.originFlight = originFlight;
		this.destinyFlight = destinyFlight;
		this.codeFlight = codeFlight;
	}
	
	public String getOriginFlight() {
		return originFlight;
	}
	public void setOriginFlight(String originFlight) {
		this.originFlight = originFlight;
	}
	public String getDestinyFlight() {
		return destinyFlight;
	}
	public void setDestinyFlight(String destinyFlight) {
		this.destinyFlight = destinyFlight;
	}
	public String getCodeFlight() {
		return codeFlight;
	}
	public void setCodeFlight(String codeFlight) {
		this.codeFlight = codeFlight;
	}
	@Override
	public String toString(){
		return "{ originFlight: " + this.originFlight + ", " +
				 "destinyFlight: " + this.destinyFlight + ", " +
				 "codeFlight: " + this.codeFlight + "}";
	}
}
