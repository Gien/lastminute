package com.lastminute.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

	private static final String OLD_FORMAT = "dd/MM/yyyy";
	
	public static String formatDate(String date){

		if (date.contains("-")){
			date = date.replace("-", "/");
		}

		if (date.contains(".")){
			date = date.replace(".", "/");
		}
		
		return date;
	}
	
	public static long getDays (String departureDate){
		SimpleDateFormat format = new SimpleDateFormat(OLD_FORMAT);
		try {
			Date date = format.parse(departureDate);

			long diff = date.getTime() - (new Date()).getTime();

			long diffDays = diff / (24 * 60 * 60 * 1000);
			return diffDays;
		} catch (ParseException e) {
			return 1;
		}
	}
	

}
