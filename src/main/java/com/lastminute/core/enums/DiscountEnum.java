package com.lastminute.core.enums;

public enum DiscountEnum {

	/***
	 * | more than 30 (i.e. >= 31)        | 80%                 |
	 * | 30 - 16                          | 100%                |
	 * | 15 - 3                           | 120%                |
	 * | less that 3 (i.e. <= 2)          | 150%                |
	 */
	DISCOUNT_03 (1.5, 3),
	DISCOUNT_16 (1.2, 16),
	DISCOUNT_30 (1, 30),
	DISCOUNT_31 (0.80, 31);
	
	double discount;
	int value;
	
	DiscountEnum(double discount, int value) {
		this.discount=discount;
		this.value=value;
	}

	public double getDiscount() {
		return discount;
	}

	public int getValue() {
		return value;
	}
	
	public static DiscountEnum getDiscountByValue(int days) {
		for (DiscountEnum type : DiscountEnum.values()) {
			if (days < type.getValue()) {
				return type;
			}
		}
		return DiscountEnum.DISCOUNT_31;
	}

}
