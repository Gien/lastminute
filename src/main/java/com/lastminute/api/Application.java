package com.lastminute.api;

import java.util.List;

import com.lastminute.core.dto.request.SearchFlightDTO;
import com.lastminute.core.dto.response.FlightResultDTO;
import com.lastminute.service.searchflight.ISearchFlightService;
import com.lastminute.service.searchflight.impl.SearchFlightService;

public class Application {
	
	static ISearchFlightService searchFlight;
	
	public static String execution(String[] args) throws Exception {
		searchFlight = new SearchFlightService();
		
		List<FlightResultDTO> results = searchFlight.findFlights(setParams(args));
		
		return printResult(results);
		
	}
	
	public static void main(String[] args) throws Exception {
		System.out.println(execution(args));
	}

	private static SearchFlightDTO setParams(String[] args) throws Exception {
		
		if (args.length < 4){
			throw new Exception ("You need 4 parameters: origin, destiny, passengers, departure date (dd/MM/yyyy)");
		}
		
		try{
			Integer.parseInt(args[2]);
		}catch(Exception e){
			throw new Exception ("The third parameter be must a number");
		}
		
		SearchFlightDTO params = new SearchFlightDTO(); 
		params.setOrigin(args[0]);
		params.setDestiny(args[1]);
		params.setPassengers(Integer.parseInt(args[2]));
		params.setDateDeparture(args[3]);
		return params;
	}

	private static String printResult(List<FlightResultDTO> results) {
		String sResult = "";
		String separator = "";
		if (!results.isEmpty()){
			sResult += "[" ;
			for (FlightResultDTO result : results){
				sResult += separator + result.toString();
				separator = ",";
			}
			sResult += "]";
		}else{
			sResult += "No flights available";
		}
		return sResult;
	}

}
